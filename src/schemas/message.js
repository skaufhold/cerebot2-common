import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import ChatUserSchema from './chatUser';

const MessageSchema = new Schema({
    body: String,
    channelName: String,
    nick: String,
    chatUser: {
        type: Schema.ObjectId,
        ref: "ChatUser",
        childPath: "messages"
    }
});

MessageSchema.pre('save', function (next) {
    this.wasNew = this.isNew;
    next();
});

MessageSchema.post('save', function(message) {
    if (this.wasNew) {
        this.model('Message').count({chatUser: message.chatUser}).then(
            count => {
                mongoose.model('ChatUser').update({_id: message.chatUser}, {
                    lastSeen: Date.now(),
                    messageCount: count
                }, function(err, chatUser) {})
            }, console.warn
        );
    }
});

MessageSchema.plugin(mongoosePaginate);

export default MessageSchema;