import mongoose, { Schema } from 'mongoose';

const ConfigurationSchema = new Schema({
    _meta: {
        name: String,
        active: { type: Boolean, default: false }
    },
    channels: { type: Array, default: ['#_cerebot2'] },
    moderation: {
        _groupName: { type: String, default: 'Moderation' },
        linksAllowed: { type: Boolean, default: false },
        strictLinkFiltering: { type: Boolean, default: false },
        defaultTimeout: { type: Number, default: 10 }
    }
});

export default ConfigurationSchema;