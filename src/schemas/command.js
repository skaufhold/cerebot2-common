import mongoose, { Schema } from 'mongoose';

const CommandSchema = new Schema({
    name: String,
    text: String
});

export default CommandSchema;