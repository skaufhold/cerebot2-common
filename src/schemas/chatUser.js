import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import Account from './chatUserAccount';
import Message from './message';

const ChatUserSchema = new Schema({
    nick: { type: String, index: true },
    role: { type: String, enum: [ 'trusted', 'moderator', 'admin' ] },
    email: String,
    signedUp: Boolean,
    account: Account,
    lastSeen: Date,
    messageCount: Number
});

ChatUserSchema.methods.findMessages = function() {
    return Message.find({chatUser: this._id});
};

ChatUserSchema.plugin(mongoosePaginate);

ChatUserSchema.pre('save', function(next) {
    if (this.isNew && this.account && this.account.password) this.account.hashPassword(next);
    else next();
});

ChatUserSchema.virtual('isModerator').get(function() {
    return this.role == 'moderator';
});

ChatUserSchema.virtual('isAdmin').get(function() {
    return this.role == 'admin';
});

ChatUserSchema.virtual('isTrusted').get(function() {
    return this.role == 'trusted';
});

export default ChatUserSchema;
