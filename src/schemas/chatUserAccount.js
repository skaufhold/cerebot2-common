/**
 * Defining a User Model in mongoose
 * Code modified from https://github.com/sahat/hackathon-starter
 */

var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var crypto = require('crypto');

// Other oauthtypes to be added

/*
 User Schema
 */

var AccountSchema = new mongoose.Schema({
    email: {type: String, index: {unique: true, sparse: true}, lowercase: true},
    password: String,
    tokens: Array,
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    google: {},
    twitch: {}
});

AccountSchema.pre('save', function(next) {
    if (!this.isModified('password')) return next();
    this.hashPassword(next);
});

/*
 Defining our own custom document instance method
 */
AccountSchema.methods = {
    comparePassword: function (candidatePassword, cb) {
        bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
            if (err) return cb(err);
            cb(null, isMatch);
        })
    },
    hashPassword: function (next) {
        var account = this;
        bcrypt.genSalt(5, function (err, salt) {
            if (err) return next(err);
            bcrypt.hash(account.password, salt, null, function (err, hash) {
                if (err) return next(err);
                account.password = hash;
                next();
            });
        });
    }
};

AccountSchema.statics = {};

export default AccountSchema;
