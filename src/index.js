
import mongoose from 'mongoose';
import ChatUserSchema from './schemas/chatUser';
import ChatUserAccountSchema from './schemas/chatUserAccount';
import CommandSchema from './schemas/command';
import ConfigurationSchema from './schemas/configuration';
import MessageSchema from './schemas/message';

export { ChatUserSchema, ChatUserAccountSchema, CommandSchema, ConfigurationSchema, MessageSchema };

export const ChatUser = mongoose.model('ChatUser', ChatUserSchema);
export const Command = mongoose.model('Command', CommandSchema);
export const Configuration = mongoose.model('Configuration', ConfigurationSchema);
export const Message = mongoose.model('Message', MessageSchema);

export function connect(mongo) {
    mongoose.set('debug', process.env.NODE_ENV === 'development');
    mongoose.connect(mongo, function (err, res) {
        if (err) {
            console.log('Error connecting to: ' + mongo + '. ' + err);
        } else {
            console.log('Succeeded connected to: ' + mongo);
        }
    });
}

// expose mongoose instance
export { mongoose };